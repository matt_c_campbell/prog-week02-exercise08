﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// namespace boolVarChangeUsrInput start
namespace boolVarChangeUsrInput
{

    // class Program start
    class Program
    {

        // method Main start
        static void Main(string[] args)
        {
            Console.WriteLine("It is cloudy today\n[t] True\n[f] False\n");
            var usrInput = Console.ReadLine();

            var reply = "";

            // if statement start
            if (usrInput == "t")
            {
                reply = "You should pack an umbrella.";
                Console.WriteLine($"{reply}\n");
            }
            else if (usrInput == "f")
            {
                reply = "Enjoy your sunny day.";
                Console.WriteLine($"{reply}\n");
            }
            else
            {
                Console.WriteLine("Err: Invalid option!\n");
            }
            

        }
        // method Main end

    }
    // class Program end

}
// namespace boolVarChangeUsrInput end
